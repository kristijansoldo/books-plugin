<?php
class Books_System_Exception extends Exception
{
    public function __construct($message = "", $code = 0, Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);

        if (!empty($message)) {
            do_action(Books_Actions::$WP_SYSTEM_ERROR, $message);
        }

    }
}
