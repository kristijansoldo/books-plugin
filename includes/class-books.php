<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       http://enterwell.net
 * @since      1.0.0
 *
 * @package    Books
 * @subpackage Books/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Books
 * @subpackage Books/includes
 * @author     Kristijan Soldo <kristijan.soldo@enterwell.net>
 */
class Books
{

    /**
     * The loader that's responsible for maintaining and registering all hooks that power
     * the plugin.
     *
     * @since    1.0.0
     * @access   protected
     * @var      Books_Loader    $loader    Maintains and registers all hooks for the plugin.
     */
    protected $loader;

    /**
     * The unique identifier of this plugin.
     *
     * @since    1.0.0
     * @access   protected
     * @var      string    $plugin_name    The string used to uniquely identify this plugin.
     */
    protected $plugin_name;

    /**
     * The current version of the plugin.
     *
     * @since    1.0.0
     * @access   protected
     * @var      string    $version    The current version of the plugin.
     */
    protected $version;

    /**
     * Define the core functionality of the plugin.
     *
     * Set the plugin name and the plugin version that can be used throughout the plugin.
     * Load the dependencies, define the locale, and set the hooks for the admin area and
     * the public-facing side of the site.
     *
     * @since    1.0.0
     */
    public function __construct()
    {

        $this->plugin_name = 'books';
        $this->version     = '1.0.0';

        $this->load_dependencies();
        $this->set_locale();
        $this->define_admin_hooks();
        $this->define_public_hooks();

    }

    /**
     * Load the required dependencies for this plugin.
     *
     * Include the following files that make up the plugin:
     *
     * - Books_Loader. Orchestrates the hooks of the plugin.
     * - Books_i18n. Defines internationalization functionality.
     * - Books_Admin. Defines all hooks for the admin area.
     * - Books_Public. Defines all hooks for the public side of the site.
     *
     * Create an instance of the loader which will be used to register the hooks
     * with WordPress.
     *
     * @since    1.0.0
     * @access   private
     */
    private function load_dependencies()
    {

        /**
         * Include constants to be used within a plugin.
         */
        require_once plugin_dir_path(dirname(__FILE__)) . 'constants.php';
        require_once plugin_dir_path(dirname(__FILE__)) . 'actions/actions.php';

        // Exceptions
        require_once BOOKS_PLUGIN_DIR . 'exceptions/class-books-model-exception.php';
        require_once BOOKS_PLUGIN_DIR . 'exceptions/class-books-system-exception.php';

        //logger

        // Interfaces
        require_once BOOKS_PLUGIN_DIR . 'interfaces/interface-books-api-class.php';
        require_once BOOKS_PLUGIN_DIR . 'interfaces/interface-books-api-service.php';

        // Custom post types
        require_once BOOKS_PLUGIN_DIR . 'post-types/class-books-movies.php';

        /**
         * The class responsible for orchestrating the actions and filters of the
         * core plugin.
         */
        require_once BOOKS_PLUGIN_DIR . 'includes/class-books-loader.php';

        /**
         * The class responsible for defining internationalization functionality
         * of the plugin.
         */
        require_once BOOKS_PLUGIN_DIR . 'includes/class-books-i18n.php';

        // WP Frontend API Controllers
        require_once BOOKS_PLUGIN_DIR . 'wp-controllers/class-wp-books-base-controller.php';
        require_once BOOKS_PLUGIN_DIR . 'wp-controllers/class-wp-books-movies-controller.php';

        /**
         * The class responsible for defining all actions that occur in the admin area.
         */
        require_once BOOKS_PLUGIN_DIR . 'admin/class-books-admin.php';

        /**
         * Options class.
         */
        require_once BOOKS_PLUGIN_DIR . 'admin/class-books-options.php';

        /**
         * The class responsible for defining all actions that occur in the public-facing
         * side of the site.
         */
        require_once BOOKS_PLUGIN_DIR . 'public/class-books-public.php';

        $this->loader = new Books_Loader();

    }

    /**
     * Define the locale for this plugin for internationalization.
     *
     * Uses the Books_i18n class in order to set the domain and to register the hook
     * with WordPress.
     *
     * @since    1.0.0
     * @access   private
     */
    private function set_locale()
    {

        $plugin_i18n = new Books_i18n();

        $this->loader->add_action('plugins_loaded', $plugin_i18n, 'load_plugin_textdomain');

    }

    /**
     * Register all of the hooks related to the admin area functionality
     * of the plugin.
     *
     * @since    1.0.0
     * @access   private
     */
    private function define_admin_hooks()
    {

        $plugin_admin = new Books_Admin($this->get_plugin_name(), $this->get_version());

        $this->loader->add_action('admin_enqueue_scripts', $plugin_admin, 'enqueue_styles');
        $this->loader->add_action('admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts');

        //create post type page
        $this->loader->add_action('init', $plugin_admin, 'create_posttype');

        //zanrovi meta box
        $this->loader->add_action('add_meta_boxes', $plugin_admin, 'zanrovi_box');

        //updateing zanrovi
        $this->loader->add_action('save_post', $plugin_admin, 'zanrovi_box_save');

        //register movies post types
        $this->loader->add_action('init', Books_Movie::class, 'register_post_type');
        //movies post type zanr
        $this->loader->add_action('add_meta_boxes', Books_Movie::class, 'zanrovi_box_movies');
        $this->loader->add_action('save_post', Books_Movie::class, 'zanrovi_box_movies_save');

        // Get Books Options class
        $books_options = Books_Options::get_instance();
        //create post type setting page
        $this->loader->add_action('admin_menu', $books_options, 'posttype_options');

        //ajax actions for zanr
        $this->loader->add_action('wp_ajax_my_action', $books_options, 'my_action_callback');
        $this->loader->add_action('wp_ajax_nopriv_my_action', $books_options, 'my_action_callback');



        // Create controllers for WordPress Frontend
        $this->create_frontend_controllers();

    }

    /**
     * Creates all Frontend API Controllers for site frontend.
     */
    private function create_frontend_controllers()
    {
        $controller = new WP_Books_Movie_Controller('movie');
        $this->loader->add_action('rest_api_init', $controller, 'register_routes');

    }

    /**
     * Register all of the hooks related to the public-facing functionality
     * of the plugin.
     *
     * @since    1.0.0
     * @access   private
     */
    private function define_public_hooks()
    {

        $plugin_public = new Books_Public($this->get_plugin_name(), $this->get_version());

        $this->loader->add_action('wp_enqueue_scripts', $plugin_public, 'enqueue_styles');
        $this->loader->add_action('wp_enqueue_scripts', $plugin_public, 'enqueue_scripts');

        //custom page template for books
        $this->loader->add_filter('page_template', $plugin_public, 'custom_page_books_template');

        //custom page template for books ajax page
        $this->loader->add_filter('page_template', $plugin_public, 'custom_page_books_ajax_template');

        //custom page for get api novies
        $this->loader->add_filter('page_template', $plugin_public, 'custom_page_get_movies_api');

        //custom page for create movies
        $this->loader->add_filter('page_template', $plugin_public, 'custom_page_create_movie');

        //get books data with ajax
        $this->loader->add_action('wp_ajax_get_books_data', $plugin_public, 'get_books_data_callback');
        $this->loader->add_action('wp_ajax_nopriv_get_books_data', $plugin_public, 'get_books_data_callback');

        //define ajax url in frontnend
        $this->loader->add_action('wp_head', $plugin_public, 'pluginname_ajaxurl');

    }

    /**
     * Run the loader to execute all of the hooks with WordPress.
     *
     * @since    1.0.0
     */
    public function run()
    {
        $this->loader->run();
    }

    /**
     * The name of the plugin used to uniquely identify it within the context of
     * WordPress and to define internationalization functionality.
     *
     * @since     1.0.0
     * @return    string    The name of the plugin.
     */
    public function get_plugin_name()
    {
        return $this->plugin_name;
    }

    /**
     * The reference to the class that orchestrates the hooks with the plugin.
     *
     * @since     1.0.0
     * @return    Books_Loader    Orchestrates the hooks of the plugin.
     */
    public function get_loader()
    {
        return $this->loader;
    }

    /**
     * Retrieve the version number of the plugin.
     *
     * @since     1.0.0
     * @return    string    The version number of the plugin.
     */
    public function get_version()
    {
        return $this->version;
    }

}
