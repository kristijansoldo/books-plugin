<?php

/**
 * Creates all tables required for autori.
 *
 * Class Movie_Autori_Activator
 */
class Movie_Autori_Activator{

    /**
     * @var string  Autori table name.
     */
    private $autori_table;

    /**
     * @var string  Default charset collate.
     */
    private $charset_collate;

    /**
     * Movie_Autori_Activator constructor.
     */
    public function __construct()
    {
        // Get global wpdb object
        global $wpdb;
        if(empty($wpdb))
            throw new Exception("No \$wpdb object during activation!");

        $this->charset_collate = $wpdb->get_charset_collate();
        $this->autori_table = $wpdb->prefix . 'autori';
    }

    /**
     * Creates all tables.
     */
    public function activate(){

        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

        $queries = [
            $this->get_autori_table_sql()
        ];

        dbDelta($queries);

    }

    /**
     * @return string   SQL Command to create autori table.
     */
    private function get_autori_table_sql(){
        
        return "CREATE TABLE {$this->autori_table}(
id mediumint(9) NOT NULL AUTO_INCREMENT,
name VARCHAR(256),
PRIMARY KEY id (id)
) {$this->charset_collate};";

    }
}