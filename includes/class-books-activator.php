<?php

/**
 * Fired during plugin activation
 *
 * @link       http://enterwell.net
 * @since      1.0.0
 *
 * @package    Books
 * @subpackage Books/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Books
 * @subpackage Books/includes
 * @author     Kristijan Soldo <kristijan.soldo@enterwell.net>
 */
class Books_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

		// Load activators
		static::load_activators();	

		// Create tables for autori
		$autori_activator = new Movie_Autori_Activator();
		$autori_activator->activate();

	}
	/**
	 * Load all activator classes.
	 */
	private static function load_activators(){

		// Base path to require
		$base = plugin_dir_path(__FILE__);

		require_once $base . 'activators/class-movie-autori-activator.php';
	}

}
