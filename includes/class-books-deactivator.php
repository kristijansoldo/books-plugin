<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://enterwell.net
 * @since      1.0.0
 *
 * @package    Books
 * @subpackage Books/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Books
 * @subpackage Books/includes
 * @author     Kristijan Soldo <kristijan.soldo@enterwell.net>
 */
class Books_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
