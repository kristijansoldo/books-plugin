<?php
// Global plugin constants
define("BOOKS_PLUGIN_DIR", plugin_dir_path(  __FILE__  ));
define("BOOKS_PLUGIN_URL", plugin_dir_url(  __FILE__  ));
define("PLUGIN_TEXTDOMAIN", "books");
