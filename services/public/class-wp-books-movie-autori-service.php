<?php

class WP_Books_Autori_Service
{

    /**
     * @var Books_Movie_Autori_Repository
     */
    private $autori_repository;

    /**
     * WP_Books_Autori_Service constructor.
     */
    public function __construct()
    {
        $this->autori_repository = new Books_Movie_Autori_Repository();
    }

    /**
     * Validates array that represents office create REST request.
     *
     * @param $r    array                               Array with office values.
     * @return      Lunch_Request_Validation_Result     Result of validation.
     */
    public function validate_wp_create_request($r)
    {

        // Create empty validation result
        $validation_result = new Lunch_Request_Validation_Result();

        // Required fields for office
        $required_fields = ['name'];

        // Check if all required fields exist
        foreach ($required_fields as $required_field) {
            if (empty($required_field)) {
                $validation_result->set_valid(false);
                $validation_result->add_message("Autor required field $required_field is empty!");
            }
        }

        // Validate name
        $autori_validation_result = $this->autori_service->validate_wp_create_request($r['name']);

        // Merge validation results into one result.
        $validation_result->merge($autori_validation_result);

        return $validation_result;

    }

    /**
     * Creates new autor from REST request.
     *
     * @param $r            array               Request data as array with predefined fields.
     * @param $send_to_api  bool                If created autor will be synchronized with remote api.
     * @return              Movie_Autor        Created autor.
     * @throws              Exception           In case of error.
     */
    public function create_from_wp_request($r, $send_to_api = true)
    {

        // Create empty autor
        $autor = new Movie_Autor();

        try {
            // Create autor name
            $autor->name = $this->autori_service->create_from_wp_request($r['name'], false);

            // Check if company already has an autor with same name !?
            if (!empty($autor->name)) {
                $autors_with_same_names = $this->autori_repository->get_by_name($autor->name);
                foreach ($autors_with_same_names as $autor_with_same_name) {
                    if ($autor_with_same_name->name == $autor->name) {
                        throw new Exception("Author with same name already exists!", 400);
                    }
                }
            }

            // Try to save autor
            $autor_saved = $this->autori_repository->save($autor);

            // If autor is not saved throw exception
            if (empty($autor_saved)) {
                do_action(Lunch_Actions::$WP_SAVE_ERROR, "Error saving autor!\r\noffice => \r\n\t" . var_export($autor, true));
                //TODO: Delete address
                throw new Exception("Error saving autor!", 500);
            }

            // Update autor
            $autor = $autor_saved;

            // Send autor to api
            if ($send_to_api) {

                // Send create autor request to api
                $api_response = Lunch_Api_Service::send_request($autor->to_api_object(), 'POST', 'autori');

                // If request is successful
                if (!Lunch_Api_Service::is_success($api_response)) {

                    do_action(Lunch_Actions::$API_ERROR, "Office API Creation failed.\r\noffice => \r\n\t"
                        . var_export($autor, true)
                        . "\r\nresponse => \r\n\t"
                        . var_export($api_response, true)
                    );

                    throw new Exception("Office API Creation failed!", 500);

                }

                // Handle api response
                $autor = $this->handle_api_response($autor, $api_response);

                // Save autor with new data
                $autor_saved = $this->autori_repository->save($autor);

            }

            return $autor_saved;

        } catch (Exception $e) {

            // If exception happened clear everything
            $this->delete_autor($autor);

            // Throw exception
            throw $e;
        }

    }

    /**
     * Handles API response.
     *
     * @param $autor       Movie_Autor    Office to update.
     * @param $api_response array           API response.
     * @return              Movie_Autor    Updated autor.
     */
    private function handle_api_response($autor, $api_response)
    {

        // Get response body.
        $response_body = json_decode($api_response['body'], true);

        // Set autor api id
        //$autor->api_id = intval($response_body['Id']);

        // Set autor
        $autor->name = intval($response_body['Name']);

        // Save autor
        $autor = $this->autori_repository->save($autor);

        return $autor;
    }

    /**
     * Deletes everything related to the autor.
     *
     * @param $autor
     */
    public function delete_autor($autor)
    {
        $this->autori_repository->delete($autor->id);
    }

}