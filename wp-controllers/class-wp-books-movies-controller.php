<?php

/**
 * Controller for Movie_Autor.
 *
 * Class WP_Books_Movie_Controller
 */
class WP_Books_Movie_Controller extends WP_Books_Base_Controller
{

    /**
     * Registers routes handled by this controller.
     */
    public function register_routes()
    {
        // Get controller namespace
        $namespace = $this->get_namespace();

        // Register create movie route
        register_rest_route(
            $namespace,
            "/" . $this->route,
            [
                'methods'             => 'POST',
                'callback'            => [$this, 'create_movie'],
            ]
        );

        // Register edit movie route
        register_rest_route(
            $namespace,
            "/edit_movie/(?P<id>\d+)",
            [
                'methods'  => 'POST',
                'callback' => [$this, 'eidt_movie'],
            ]
        );

        // Register route for all movies
        register_rest_route(
            $namespace,
            "/get_all_movies",
            [
                'methods'  => 'GET',
                'callback' => [$this, 'get_all_movies'],
            ]
        );

        // Register route for movie details
        register_rest_route(
            $namespace,
            "/get_movie_details/(?P<id>\d+)",
            [
                'methods'  => 'GET',
                'callback' => [$this, 'get_movie_details'],
            ]
        );

        // Regiser route for deleting movie
        register_rest_route(
            $namespace,
            "/delete_movie/(?P<id>\d+)",
            [
                'methods'  => 'DELETE',
                'callback' => [$this, 'delete_movie'],
            ]
        );
    }

    /**
     * @param $r    array
     * @return WP_REST_Response
     */
    public function get_all_movies()
    {
        global $post;

        $args = array(
            'numberposts' => 10,
            'post_type'   => 'movie',
            'post_status' => 'publish',
        );
        $posts = get_posts($args);

        if (empty($posts)) {
            return null;
        }

        $return = array();

        foreach ($posts as $post) {
            $return[] = array(
                'ID'        => $post->ID,
                'title'     => $post->post_title,
                'permalink' => get_permalink($post->ID),
                'content'   => $post->post_content,
                'zanr'      => get_post_meta($post->ID, 'zanr', true),
                'autor'     => get_post_meta($post->ID, 'autor', true),
            );
        }
        $response = new WP_REST_Response($return);
        return $response;

    }

    /**
     * @param $request WP_REST_Request
     * @return WP_REST_Response
     */
    public function get_movie_details($request)
    {
        $post_id     = $request['id'];
        $post_output = get_post($post_id);
        $return      = array();
        $return[]    = array(
            'ID'        => $post_output->ID,
            'title'     => $post_output->post_title,
            'content'   => $post_output->post_content,
            'permalink' => get_permalink($post_output->ID),
            'zanr'      => get_post_meta($post_output->ID, 'zanr', true),
            'autor'     => get_post_meta($post_output->ID, 'autor', true),
        );
        $response = new WP_REST_Response($return);
        return $response;

    }

    /**
     * @param $r    array
     * @return WP_REST_Response
     */
    public function create_movie()
    {
        $title   = $_REQUEST['post_title'];
        $content = $_REQUEST['post_content'];
        $zanr    = $_REQUEST['zanr'];
        $autor   = $_REQUEST['autor'];

        // Create post object
        $my_post = array(
            'post_title'   => wp_strip_all_tags($title),
            'post_content' => $content,
            'post_status'  => 'publish',
            'post_author'  => 1,
            'post_type'    => 'movie',
        );

        // Insert the post into the database with meta data
        $update__post = wp_insert_post($my_post);

        //set meta fields in array
        $meta_fields = array('zanr' => $zanr, 'autor' => $autor);
        
        //update post data 
        foreach ($meta_fields as $meta_field => $meta_value) {
            $updt_post_meta = update_post_meta($update__post, $meta_field, $meta_value);
        }

        $response = new WP_REST_Response($updt_post_meta);

        return $response;

    }

    /**
     * @param $request WP_REST_Request
     * @return WP_REST_Response
     */
    public function eidt_movie($request)
    {
        //get data 
        $post_ID = $_REQUEST['ID'];
        $title   = $_REQUEST['post_title'];
        $content = $_REQUEST['post_content'];
        $zanr    = $_REQUEST['zanr'];
        $autor   = $_REQUEST['autor'];

        // Create post object
        $my_post = array(
            'ID'           => $post_ID,
            'post_title'   => wp_strip_all_tags($title),
            'post_content' => $content,
            'post_status'  => 'publish',
            'post_author'  => 1,
            'post_type'    => 'movie',
        );

        // Insert the post into the database
        $update_post = wp_update_post($my_post);

        //set meta fileds in array 
        $meta_fields = array('zanr' => $zanr, 'autor' => $autor);

        //foreach for update post 
        foreach ($meta_fields as $meta_field => $meta_value) {
            $updt_post_meta = update_post_meta($update_post, $meta_field, $meta_value);
        }

        //response
        $response = new WP_REST_Response($updt_post_meta);

        return $response;

    }

    /**
     * @param $request WP_REST_Request
     * @return WP_REST_Response
     */
    public function delete_movie($request)
    {

        //get post id
        $post_id = $request['id'];

        //get data
        $delete_success = wp_delete_post($post_id);

        $response = new WP_REST_Response($delete_success);
        return $response;
    }
}
