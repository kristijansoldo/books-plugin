<?php 

/**
 * Controller for Movie_Autor.
 *
 * Class WP_Books_Movie_Autori_Controller
 */
class WP_Books_Movie_Autori_Controller extends WP_Books_Base_Controller
{

    /**
     * Registers routes handled by this controller.
     */
    public function register_routes()
    {
        // Get controller namespace
        $namespace = $this->get_namespace();

        // Register create author route
        register_rest_route(
            $namespace,
            "/" . $this->route,
            [
                'methods' => 'POST',
                'callback' => [$this, 'create_autor'],
                'permission_callback' => [$this, 'validate_nonce'],
            ]
        );

        // Edit author - make author confirmed
        register_rest_route(
            $namespace ,
            "/" . $this->route ."/(?P<id>\d+)",
            [
                'methods'               => 'POST',
                'callback'              => [$this, 'edit_autor'],
                'permission_callback'   => [$this, 'validate_nonce'],
                'args'                  => [
                    'required' => true
                ]
            ]
        );
    }


    /**
     * Create new author handler.
     *
     * @param $r    array
     * @return WP_REST_Response
     */
    public function create_autor($r)
    {

        // Create autors service
        $autori_service = new WP_Books_Autori_Service();

        // Validate request
        $validation_result = $autori_service->validate_wp_create_request($r);

        // If request is not valid return why.
        if (!$validation_result->is_valid()) {
            return new WP_REST_Response("Request not valid. Details: " . $validation_result->get_message(), 400);
        }

        try {
            // Create author from wp request
            $autor = $autori_service->create_from_wp_request($r);

            // Return response
            return new WP_REST_Response($autor, 200);

        } catch (Exception $e) {

            // If any exception happened return response with exception data.
            return new WP_REST_Response($e->getMessage(), $e->getCode());
        }

    }

    /**
     * @param $r    array
     */
    public function edit_autor($r){

        // Autori service
        $autori_service = new WP_Books_Autori_Service();

    }
}