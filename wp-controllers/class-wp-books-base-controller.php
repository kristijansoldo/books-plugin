<?php

abstract class WP_Books_Base_Controller
{

    /**
     * @var string  Base Controller's route.
     */
    protected $base_route = "wp-books-frontend";

    /**
     * @var string  WP APi version
     */
    protected $version = "v1";

    /**
     * @var string  WP APi version
     */
    protected $app_id = "6n1jfW6HZ0RK8k4v5UJrRHyR6Ib00UpG";

    /**
     * @var string  Current controller route.
     */
    protected $route;

    /**
     * WP_Books_Base_Controller constructor.
     * @param $route
     */
    public function __construct($route)
    {
        $this->route = $route;
    }

    abstract public function register_routes();

    /**
     * @param $request WP_REST_Request
     * @return bool
     */
    public function validate_nonce($request)
    {

        // Get request body
        $data = $request->get_body_params();

        return !empty($data) && !empty($data['_wpnonce']) && wp_verify_nonce($data['_wpnonce'], 'wp_rest');
    }

    //validate users

    /**
     * @param $request WP_REST_Request
     * @return bool
     */
    public function validate_user($request)
    {

    }

    /**
     * Get current controller's namespace.
     *
     * @return string   This controller's namespace.
     */
    public function get_namespace()
    {
        return implode("/", [$this->base_route, $this->version]);
    }

}
