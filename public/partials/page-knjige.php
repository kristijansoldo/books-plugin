<?php
	//get header
	get_header(); 

	$args = array( 'post_type' => 'books', 'posts_per_page' => 10 );
	$loop = new WP_Query( $args );
	
	//start the loop
	 if ( $loop->have_posts() ) : while ( $loop->have_posts() ) : $loop->the_post(); ?>

 	<?php if ( in_category( '3' ) ) : ?>
 		<div class="post-cat-three">
 	<?php else : ?>
 		<div class="post">
 	<?php endif; ?>


 	<!-- Display the Title as a link to the Post's permalink. -->

 	<h2><a href="<?php the_permalink(); ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>

 	<!-- Display the Post's content in a div box. -->

 	<div class="entry">
 		<?php the_content(); ?>
 	</div>
 	<small>
 		<?php 
 		$zanr = get_post_meta( get_the_ID(), 'zanr', true );
 		// Check if the custom field has a value.
		if (!empty($zanr)) : ?>
		   <p><b>Zanr:</b><?php echo $zanr; ?></p>
		<?php endif; ?>   
	</small>

 	<!-- Stop The Loop (but note the "else:" - see next line). -->

 <?php endwhile; else : ?>


 	<!-- The very first "if" tested to see if there were any Posts to -->
 	<!-- display.  This "else" part tells what do if there weren't any. -->
 	<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>


 	<!-- REALLY stop The Loop. -->
 <?php endif; ?>

<?php get_footer(); ?>
