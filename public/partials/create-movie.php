<?php get_header();
$zanrovi_get_option = get_option('zanorovi_novi');
?>
<div class="content-area">
    <form id="post-form">
        <label>
            Ime filma
        </label>
        <input name="post_title" type="text" class="form-control" />
        <label>
            Zanr
        </label>
        
        <select name="zanr" class="form-control">
            <?php 
                foreach ($zanrovi_get_option as $value) {
                    echo '<option value="'.$value.'">'.$value.'</option>';
                }
            ?>
        </select>
        <label>
            Autor
        </label>
        <input name="autor" type="text" class="form-control" />
        <label>
            Opis filma 
        </label>

        <textarea name="post_content" /></textarea>
        <button type="button" name="submit" id="create_movie_btn">Create</button>
    </form>
</div>
<?php get_footer();?>