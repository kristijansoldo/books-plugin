<?php get_header(); ?>
<div class="content-area">
    <div class="movies"></div>
</div>
<div class="sidebar widget-area">
    <form id="editForm">
        <input type="hidden" value="" id="ID" name="ID">
        <label>Naslov</label>
        <input type="text" id="title" name="post_title" value="" class="form-control" >
        <label>Autor</label>
        <input type="text" id="autor" name="autor" value="" class="form-control" >
        <label>Zanr</label>
        <input type="text" id="zanr" name="zanr" value="" class="form-control" >
        <label>Opis filma</label>
        <textarea id="content" name="post_content"></textarea>
        <br><br>
        <button type="button" class="btn btn-primary" id="btn_edit_save">Izmjeni</button>
    </form>
</div>
<?php get_footer(); ?>