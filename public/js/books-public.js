(function ($) {
    'use strict';
    /**
     * All of the code for your public-facing JavaScript source
     * should reside in this file.
     *
     * Note: It has been assumed you will write jQuery code here, so the
     * $ function reference has been prepared for usage within the scope
     * of this function.
     *
     * This enables you to define handlers, for when the DOM is ready:
     *
     * $(function() {
     *
     * });
     *
     * When the window is loaded:
     *
     * $( window ).load(function() {
     *
     * });
     *
     * ...and/or other possibilities.
     *
     * Ideally, it is not considered best practise to attach more than a
     * single DOM-ready or window-load handler for a particular page.
     * Although scripts in the WordPress core, Plugins and Themes may be
     * practising this, we should strive to set a better example in our own work.
     */
    $(document).ready(function () {


        //get books data ajax
        $.ajax({
            method: 'POST',
            url: ajaxurl,
            data: {
                action: 'get_books_data',
            },
            success: function (response) {
                $('#get_data').html(response);
            },
            error: function () {
                alert('Greška!!');
            }
        });


        //api conf
        var link = 'http://localhost:8888/enterwell/wordpress/wp-json/wp-books-frontend';
        var version = 'v1';


        //get movies data with api
        $.getJSON(link + '/' + version + '/get_all_movies', function (data) {
            $.each(data, function (key, val) {
                $('.movies').append('<div class="movie movie' + val.ID + '"><a href="#' + val.ID + '" class="details" data-id="' + val.ID + '"><h4>' + val.title + '</h4></a><a class="delete" href="#" data-id="' + val.ID + '">Delete</a> <a class="edit_btn" href="#" data-id="' + val.ID + '">Edit</a>' + '<p id="' + val.ID + '">' + val.content + '</p></div>');
            });
        });

        //get movies details on click
        $('.details').live('click', function () {
            var id = $(this).attr("data-id");
            $.getJSON(link + '/' + version + '/get_movie_details/' + id, function (response) {
                $.each(response, function (key, val) {
                    console.log(val.zanr);
                    $('#' + id).append('<span class="movie_details"><br><b>Žanr:</b>' + val.zanr + '<br><b>Autor:</b>' + val.autor + '</span>');
                });
            });
            return false;
        });

        //delete movie by id
        $('.delete').live('click', function () {
            var id = $(this).attr('data-id');
            //if confirmed, delete movie
            if (confirm("Sigurno želite izbrisati?") == true) {
                $.ajax({
                    url: link + '/' + version + '/delete_movie/' + id,
                    method: 'DELETE',
                    success: function (data) {
                        console.log(data);
                        $('.movie' + id).remove();
                    },
                });
            } else {
                return false;
            }
        });

        //create movie
        $('#create_movie_btn').click(function () {
            var postForm = $('#post-form');
            $.ajax({
                url: link + '/' + version + '/movie',
                method: 'POST',
                data: postForm.serialize(),
                crossDomain: true,
                success: function (data) {
                    console.log(data);
                    alert('Kreirano');
                    //reset form after success
                    $("#post-form")[0].reset();
                },
                error: function (response) {
                    var error_message = JSON.parse(response.responseText);
                    alert(error_message.message);
                },
            });
        });

        //get movie details in form
        $('.edit_btn').live('click', function () {

            //reset data in form if I click edit on different movie
            if ($('#editForm').css('display') == 'block') {
                $('#editForm')[0].reset();
                $('textarea#content').val('');
            }
            //get the movie id
            var id = $(this).attr('data-id');

            //show edit form onClick
            $('#editForm').show();

            //get movie details and append in inputs
            $.getJSON(link + '/' + version + '/get_movie_details/' + id, function (response) {
                $('#ID').val($('#ID').val() + id);
                $('#title').val($('#title').val() + response[0].title);
                $('#autor').val($('#autor').val() + response[0].autor);
                $('#zanr').val($('#zanr').val() + response[0].zanr);
                $('textarea#content').val(response[0].content);
            });
            return false;
        });

        //edit movie 
        $('#btn_edit_save').click(function () {
            var id = $('#ID').val();
            var editForm = $('#editForm');
            $.ajax({
                url: link + '/' + version + '/edit_movie/' + id,
                method: 'POST',
                data: editForm.serialize(),
                crossDomain: true,
                success: function (data) {
                    //alert('Izmjenjeno');
                    //reset form after success
                    $("#editForm")[0].reset();
                    //reload page after insert
                    location.reload();
                },
                error: function (response) {
                    var error_message = JSON.parse(response.responseText);
                    alert(error_message.message);
                },
            });
        });
    });
})(jQuery);