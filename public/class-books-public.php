<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://enterwell.net
 * @since      1.0.0
 *
 * @package    Books
 * @subpackage Books/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Books
 * @subpackage Books/public
 * @author     Kristijan Soldo <kristijan.soldo@enterwell.net>
 */
class Books_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */

	
	//defining custom page template for books
	function custom_page_books_template( $page_template ){
	    if ( is_page( 'books-page' ) ) {
	        $page_template = BOOKS_PLUGIN_DIR . 'public/partials/page-knjige.php';
	    }
	    return $page_template;
	}

	//defining custom page template for books AJAX
	function custom_page_books_ajax_template( $page_template ){
	    if ( is_page( 'books-page-ajax' ) ) {
	        $page_template = BOOKS_PLUGIN_DIR . 'public/partials/page-knjige-ajax.php';
	    }
	    return $page_template;
	}
	function custom_page_get_movies_api ($page_template){
		if (is_page( 'get-all-movies-api' )) {
			$page_template = BOOKS_PLUGIN_DIR . 'public/partials/get-all-movies.php';
		}
		return $page_template;
	}

	function custom_page_create_movie($page_template){
		if (is_page( 'create-movie' )) {
			$page_template = BOOKS_PLUGIN_DIR . 'public/partials/create-movie.php';
		}
		return $page_template;
	}


//
	//define ajaxurl in frontend
	function pluginname_ajaxurl() { ?>
		<script type="text/javascript">
			var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
		</script>
	<?php }


	//get books data with ajax
	function get_books_data_callback () {
		$args = array( 'post_type' => 'books', 'posts_per_page' => 10 );
		$loop = new WP_Query( $args );
		//start the loop
	 	if ( $loop->have_posts() ) : while ( $loop->have_posts() ) : $loop->the_post(); ?>
		<!-- Display the Title as a link to the Post's permalink. -->
		<h2>
			<a href="<?php the_permalink(); ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>">
				<?php the_title(); ?>
			</a>
		</h2>
	 	<?php the_content(); ?>

	 	<small>
	 		<?php 
		 		$zanr = get_post_meta( get_the_ID(), 'zanr', true );
		 		// Check if the custom field has a value.
				if (!empty($zanr)) : ?>
				   <p><b>Zanr:</b><?php echo $zanr; ?></p>
			<?php endif; ?>   
		</small>
	<?php
	 	endwhile; else :
	 		_e( 'Sorry, no posts matched your criteria.' );
	 	endif;
	 	/*

	 	$args = array( 
	    'post_type' => 'books', 
	    'post_status' => 'publish', 
	    'nopaging' => true 
		);
		$query = new WP_Query( $args ); // $query is the WP_Query Object
		$posts = $query->get_posts();   // $posts contains the post objects

		$output = array();
		foreach( $posts as $post ) {    // Pluck the id and title attributes
		    $output[] = array( 'id' => $post->ID, 'title' => $post->post_title, 'zanr' => get_post_meta( $post->ID, 'zanr', true ), );
		}
		echo json_encode( $output );
		*/
	}



	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Books_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Books_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/bootstrap.min.css', array(), $this->version, 'all' );
	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Books_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Books_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/books-public.js', array( 'jquery' ), $this->version, false );

	}

}
