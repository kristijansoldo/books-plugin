<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://enterwell.net
 * @since      1.0.0
 *
 * @package    Books
 * @subpackage Books/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Books
 * @subpackage Books/admin
 * @author     Kristijan Soldo <kristijan.soldo@enterwell.net>
 */
class Books_Options {

	/**
	 * Singleton instance of this class.
	 *
	 * @var     Books_Options $instance
	 */
	private static $instance;

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */


 		//Function for custom post type settings
		function posttype_options() {
	   		 add_submenu_page(
	        'edit.php?post_type=books',
	        __( 'Books Options', 'textdomain'),
	        __( 'Books Options', 'textdomain'),
	        'manage_options',
	        'books-options',
	        [$this, 'books_options_callback']
    		);
		}

		//display submenu custom post type settings
		function books_options_callback() {
			//dohvaćanje dropdowna iz optionsa
  			$zanrovi_get_option = get_option('zanorovi_novi');
			include 'partials/books-admin-display.php';
			
		}

		function my_action_callback() {
			//get the post data
			$zanrovi = array();
			if (!empty($_REQUEST['zanrovi'])) {
				$zanrovi = $_REQUEST['zanrovi'];
    			update_option('zanorovi_novi', $zanrovi );
			}
			
    		//echo $zanr; 
    		wp_die();	
		}








	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Books_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Books_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/books-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Books_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Books_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/books-admin.js', array( 'jquery' ), $this->version, false );

	}

	/**
	 * Get instance method in singleton.
	 *
	 * If there is no instance creates a new one, or returns current.
	 *
	 * @return Lunch_Settings           Instance of lunch settings class.
	 */
	public static function get_instance() {

		// If there is no instance created
		if ( static::$instance == null ) {

			// Create new instance
			static::$instance = new Books_Options();
		}

		return static::$instance;
	}


}
