(function ($) {
    'use strict';
    /**
     * All of the code for your admin-facing JavaScript source
     * should reside in this file.
     *
     * Note: It has been assumed you will write jQuery code here, so the
     * $ function reference has been prepared for usage within the scope
     * of this function.
     *
     * This enables you to define handlers, for when the DOM is ready:
     *
     * $(function() {
     *
     * });
     *
     * When the window is loaded:
     *
     * $( window ).load(function() {
     *
     * });
     *
     * ...and/or other possibilities.
     *
     * Ideally, it is not considered best practise to attach more than a
     * single DOM-ready or window-load handler for a particular page.
     * Although scripts in the WordPress core, Plugins and Themes may be
     * practising this, we should strive to set a better example in our own work.
     */
    //ajax
    $(document).ready(function () {
        $("#add_input").click(function () {
            $('.polje').after('<div class="field"> <input autocomplete="off" class="input input_array" id="field1" name="zanrovi[]" type="text" placeholder="Unesi žanr" data-items="8"/>  </div>');
        });
        $('#submit').click(function () {
            //gat array
            var $zanrovi = [];
            $(".input_array").each(function () {
                $zanrovi.push($(this).val());
            });
            console.log($zanrovi);
            $.ajax({
                method: 'POST',
                url: ajaxurl,
                data: {
                    action: 'my_action',
                    zanrovi: $zanrovi,
                },
                success: function () {
                    $('#uspjesno').fadeIn();
                },
                error: function () {
                    alert('Greška!!');
                }
            });
            return false;
        });

        //remove zanr
        $('.remove').click(function () {
            var id = $(this).attr("data-id");
            $('#' + id).remove();
        });

        //delete used values in select-options form
        var usedNames = {};
        $("select[name='zanr'] > option").each(function () {
            if (usedNames[this.text]) {
                $(this).remove();
            } else {
                usedNames[this.text] = this.value;
            }
        });

    });
})(jQuery);