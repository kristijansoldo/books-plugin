<?php
ob_start();
/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://enterwell.net
 * @since      1.0.0
 *
 * @package    Books
 * @subpackage Books/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Books
 * @subpackage Books/admin
 * @author     Kristijan Soldo <kristijan.soldo@enterwell.net>
 */
class Books_Admin
{

    /**
     * The ID of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $plugin_name    The ID of this plugin.
     */
    private $plugin_name;

    /**
     * The version of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $version    The current version of this plugin.
     */
    private $version;

    /**
     * Initialize the class and set its properties.
     *
     * @since    1.0.0
     * @param      string    $plugin_name       The name of this plugin.
     * @param      string    $version    The version of this plugin.
     */
    public function __construct($plugin_name, $version)
    {

        $this->plugin_name = $plugin_name;
        $this->version     = $version;

    }

    /**
     * Register the stylesheets for the admin area.
     *
     * @since    1.0.0
     */

    // Custom post type function for books
    public function create_posttype()
    {

        register_post_type(
            'books',
            // CPT Options
            array(
                'labels'      => array(
                    'name'          => __('Books'),
                    'singular_name' => __('Book'),
                ),
                'public'      => true,
                'has_archive' => true,
                'rewrite'     => array('slug' => 'books'),
                'supports'    => array('title', 'editor', 'thumbnail' /*, 'excerpt', 'comments'*/),

            )
        );
    }

    //dodavanje boxa za informacije
    public function zanrovi_box()
    {
        add_meta_box(
            'zanrovi_box',
            __('Žanr i Autor', 'myplugin_textdomain'),
            [$this, 'zanrovi_box_content'],
            'books',
            'side',
            'high'
        );
    }

    public function zanrovi_box_content($post)
    {
        global $post;
        wp_nonce_field(plugin_basename(__FILE__), 'zanrovi_box_content_nonce');

        //dohvaćanje podataka za ispisvanje u value
        $zanr  = get_post_meta($post->ID, 'zanr', true);
        $autor = get_post_meta($post->ID, 'autor', true);

        //dohvaćanje dropdowna iz optionsa
        $zanrovi_get_option = get_option('zanorovi_novi');
        //pozivanje template-a boxa
        include 'partials/books-zanr-box.php';
    }

    public function zanrovi_box_save($post_id)
    {
        //autosejving
        if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
            return;
        }

        if (!empty($_POST['zanrovi_box_content_nonce'])) {
            if (!wp_verify_nonce($_POST['zanrovi_box_content_nonce'], plugin_basename(__FILE__))) {
                return;
            }

            if ('page' == $_POST['post_type']) {
                if (!current_user_can('edit_page', $post_id)) {
                    return;
                }

            } else {
                if (!current_user_can('edit_post', $post_id)) {
                    return;
                }
            }

            //dohvaćanje iz forme
            $zanr  = $_POST['zanr'];
            $autor = $_POST['autor'];

            //apdejtanje u bazu
            update_post_meta($post_id, 'zanr', $zanr);
            update_post_meta($post_id, 'autor', $autor);
        }
    }

    public function enqueue_styles()
    {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in Books_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Books_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */

        wp_enqueue_style($this->plugin_name, plugin_dir_url(__FILE__) . 'css/books-admin.css', array(), $this->version, 'all');

    }

    /**
     * Register the JavaScript for the admin area.
     *
     * @since    1.0.0
     */
    public function enqueue_scripts()
    {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in Books_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Books_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */

        wp_enqueue_script($this->plugin_name, plugin_dir_url(__FILE__) . 'js/books-admin.js', array('jquery'), $this->version, false);

    }

}
