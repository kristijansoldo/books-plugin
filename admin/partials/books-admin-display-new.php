<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://enterwell.net
 * @since      1.0.0
 *
 * @package    Books
 * @subpackage Books/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
<h1>Unesi zanr</h1>
<div class="container">
	<div class="row">
    <div id="zanrovi_get_data">
    </div>
        <div class="control-group" id="fields">
            <label class="control-label" for="field1">Žanr</label>
            <div class="controls" id="profs"> 
               <?php 
                foreach ($zanrovi_get_option as $key => $zanr_value) { 
                    //zbrajanje keya sa 1 radi javascripta
                    $field = $key + 1 ;                    
                    ?>
                    <div id="field">
                        <input autocomplete="off" class="input input_array" id="field<?php echo $field; ?>" name="zanrovi[<?php echo $key; ?>]" type="text" placeholder="Unesi žanr" data-items="8" value="<?php echo $zanr_value; ?>"/>
                            <button id="remove<?php echo $field; ?>" class="button-primary" type="button">x</button><br>
                    </div>
                <?php } ?>
                   <div id="field" class="polje">
                    	<input autocomplete="off" class="input input_array" id="field1" name="zanrovi" type="text" placeholder="Unesi žanr" data-items="8"/>
                    		
                    </div>
                    <button class="button-primary" id="submit">Spremi promjene</button>&nbsp;<button id="add_input" class="button-primary" type="button">+</button>
                    <div id="uspjesno">Uspješno ubačeno :) </div>
            <br>
            </div>
        </div>
	</div>
</div>



