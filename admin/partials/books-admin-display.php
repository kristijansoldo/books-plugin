<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://enterwell.net
 * @since      1.0.0
 *
 * @package    Books
 * @subpackage Books/admin/partials
 */
?>

    <!-- This file should primarily consist of HTML with a little bit of PHP. -->
    <h1>Unesi zanr</h1>
    <div class="container">
        <div class="row">
            <div id="zanrovi_get_data">
            </div>
            <div class="control-group" id="fields">
                <label class="control-label" for="field1">Žanr</label>
                <div class="controls" id="profs">
                    <?php 
                    foreach ($zanrovi_get_option as $key => $zanr_value) :
                    //zbrajanje keya sa 1 radi javascripta
                    $field = $key + 1 ;                    
                    ?>

                    <div id="<?php echo $key; ?>" class="field">
                        <input autocomplete="off" class="input input_array" name="zanrovi[]" type="text" placeholder="Unesi žanr" value="<?php echo $zanr_value; ?>"
                        />
                        <button class="button-primary remove" type="button" data-id="<?php echo $key; ?>">x</button><br>
                    </div>
                    <?php endforeach; ?>
                    <div class="field polje">
                        <input autocomplete="off" class="input input_array" id="field1" name="zanrovi" type="text" placeholder="Unesi žanr" />
                    </div>
                    <button class="button-primary" id="submit">Spremi promjene</button>&nbsp;<button id="add_input" class="button-primary"
                        type="button">+</button>
                    <div id="uspjesno">Uspješno ubačeno :) </div>
                    <div id="prikazi"></div>
                    <br>
                </div>
            </div>
        </div>
    </div>