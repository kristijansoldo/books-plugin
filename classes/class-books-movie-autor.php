<?php
/**
 * Class Movie_Autor
 */
class Movie_Autor implements Books_API_Class
{

    /**
     * @var int
     */
    public $id;

    /**
     * @var
     */
    public $name;

    /**
     * Movie_Autor constructor.
     * @param bool $row
     */

    public function __construct($row = false)
    {
        // Empty constructor - return
        //$this->api_id = 0;
        if ($row == false) {
            return;
        }

        // Set id
        $this->id = intval($row['id']);

        // Set api id
        //if(!empty($row['api_id']))
        //  $this->api_id = $row['api_id'];

        // Set name
        if (!empty($row['name'])) {
            $this->name = $row['name'];
        }

    }

    /**
     * @return  array   Array representation of api object.
     */
    public function to_api_object()
    {
        $autor = [
            //'Id' => $this->api_id,
            'Id'   => $this->id,
            'Name' => $this->name,
        ];

        return $autor;
    }
}
