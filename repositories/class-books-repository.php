<?php
/**
 * Created by PhpStorm.
 * User: Matej
 * Date: 3.8.2016.
 * Time: 12:47
 */

/**
 * Abstract repository class to be inherited by simple
 * books repositories.
 *
 * Class Books_Repository
 */
abstract class Books_Repository
{
    /**
     * @var wpdb    WordPress database manipulation object.
     */
    protected $db;

    /**
     * @var string  Table name of the repository.
     */
    protected $table_name;

    /**
     * @var array   Allowed formats for variables in table.
     */
    protected $allowed_formats;

    /**
     * Books_Repository constructor.
     *
     * @param $table_name   string      Table name for repository.
     * @throws Exception
     */
    public function __construct($table_name)
    {
        // Get wpdb from globals.
        global $wpdb;

        // If there is no wpdb throw exception - cannot use repository here
        if(empty($wpdb))
            throw new Exception("No \$wpdb here!");

        // Set db
        $this->db = $wpdb;

        // Set table name
        $this->table_name = $this->db->prefix . $table_name;

        // Set allowed formats
        $this->allowed_formats = ['%d', '%s'];
    }

    /**
     * Constructs object instance from table row.
     *
     * @param $row      array
     * @return          mixed
     */
    abstract function _construct_from_row($row);

    /**
     * Returns single object (constructed by _construct_from_row function)
     * specified by field name and value.
     *
     * @param $field_name       string          Database field name (column name)
     * @param $field_value      mixed           Column value.
     * @param $field_format     string          Format of value.
     * @return                  bool|mixed      False if no such row, instance of the object if row exists.
     */
    protected function _get_single_by_field($field_name, $field_value, $field_format)
    {
        $field_format = $this->_get_valid_field_format($field_format);

        // Prepare sql
        $sql = $this->db->prepare("SELECT * FROM {$this->table_name} WHERE $field_name = $field_format", $field_value);

        // Get row
        $row = $this->db->get_row($sql, ARRAY_A);

        if(empty($row))
            return false;

        return $this->_construct_from_row($row);
    }

    /**
     * Returns array of objects (constructed by _construct_from_row function)
     * specified by field name and value.
     *
     * @param $field_name       string          Database field name (column name)
     * @param $field_value      mixed           Column value.
     * @param $field_format     string          Format of value.
     * @return                  array           False if no such row, instance of the object if row exists.
     */
    protected function _get_all_by_field($field_name, $field_value, $field_format)
    {
        $results = [];

        $field_format = $this->_get_valid_field_format($field_format);

        // Prepare sql
        $sql = $this->db->prepare("SELECT * FROM {$this->table_name} WHERE $field_name = $field_format", $field_value);

        // Get row
        $rows = $this->db->get_results($sql, ARRAY_A);

        if(empty($rows))
            return $results;

        foreach ($rows as $row)
            $results[] = $this->_construct_from_row($row);

        return $results;
    }

    /**
     * Return all objects in database.
     *
     * @return  array       Array of all objects in the db.
     */
    protected function _get_all()
    {
        $results = [];

        $rows = $this->db->get_results("SELECT * FROM {$this->table_name}", ARRAY_A);

        if(empty($rows))
            return $results;

        foreach ($rows as $row)
            $results[] = $this->_construct_from_row($row);

        return $results;
    }

    /**
     * Deletes row in database specified by field name and value.
     *
     * @param $field_name       string          Database field name (column name)
     * @param $field_value      mixed           Column value.
     * @param $field_format     string          Format of value.
     * @return                  bool            True if successful, false otherwise.
     */
    protected function _delete_row_by_field($field_name, $field_value, $field_format)
    {
        $field_format = $this->_get_valid_field_format($field_format);

        $res = $this->db->delete($this->table_name, [$field_name => $field_value], $field_format);

        return $res !== false;
    }

    /**
     * Checks if field format is valid, if it is not returns
     * default format.
     *
     * @param   $field_format       string      Desired field format.
     * @return                      string      Valid field format.
     */
    private function _get_valid_field_format($field_format)
    {
        // If format is not valid - set it to string
        if(!in_array($field_format, $this->allowed_formats))
            $field_format = '%s';

        return $field_format;
    }
}