<?php
/**
 * Repository for CRUD operation for Movie_Autor objects.
 *
 * Class Books_Movie_Autori_Repository
 */
class Books_Movie_Autori_Repository extends Books_Repository
{

    /**
     * Books_Movie_Autori_Repository constructor.
     *
     * @param       bool|wpdb $wpdb WordPress wpdb object.
     * @throws      Exception               If wpdb cannot be accessed.
     */
    public function __construct($wpdb = false)
    {
        parent::__construct('autori');
    }

    /**
     * Constructs object instance from table row.
     *
     * @param $row      array
     * @return          mixed
     */
    public function _construct_from_row($row)
    {
        return new Movie_Autor($row);
    }

    /**
     * Gets instance of the object.
     *
     * Returns instance of the object if instance exists in database,
     * if not returns false.
     *
     * @param       $id         int                             Id of object to get.
     * @return                  Movie_Autor|false              Object we are trying to get.
     */
    public function get($id)
    {
        return $this->_get_single_by_field('id', $id, '%d');
    }

    /**
     * Gets all offices in the system.
     *
     * @return array
     */
    public function get_all()
    {
        return parent::_get_all();
    }

    /**
     * Searches for authors by author name.
     *
     * @param       $q      string                  Query string.
     * @return              array<Movie_Autor>    Array of companies whose name matches query string.
     */
    public function get_by_name($q)
    {

        // Initialize results array
        $results = [];

        // Prepare sql
        $sql = $this->db->prepare("SELECT * FROM {$this->table_name} WHERE name LIKE %s", '%' . $q . '%');

        // Get rows
        $rows = $this->db->get_results($sql, ARRAY_A);

        // If rows not empty fill results with Movie_Autor objects.
        if (!empty($rows)) {
            foreach ($rows as $row) {
                $results[] = new Movie_Autor($row);
            }

        }

        return $results;
    }

    /**
     * Deletes object from database.
     *
     * @param $id
     */
    public function delete($id)
    {
        if ($id == 0) {
            return;
        }

        $this->_delete_row_by_field('id', $id, '%d');
    }

    /**
     * Saves object to database.
     *
     * If object does not exist creates new, if it is updates current value.
     *
     * @param       $autor     Movie_Autor          Object to save.
     * @return                   Movie_Autor|false    Saved object
     * @throws                   Exception              If saving invalid object.
     */
    public function save($autor)
    {

        $this->validate($autor);

        $data = ['values' => [], 'formats' => []];

        // Set api id
       /* if (!empty($autor->api_id)) {
            $data['values']['api_id'] = $autor->api_id;
            $data['formats'][] = '%d';
        }*/

        // Set name
        if (!empty($autor->name)) {
            $data['values']['name'] = $autor->name;
            $data['formats'][] = '%s';
        }

        // Create new
        if (empty($autor->id)) {

            // Tries to get existing author with same name
            $existing_autor = $this->get_by_name($autor->name);

            if (!empty($existing_autor))
                throw new Books_Model_Exception("Author with same name already exists!");

            $res = $this->db->insert($this->table_name, $data['values'], $data['formats']);

            if (!empty($res))
                $autor->id = $this->db->insert_id;

        } else {
            //Update
            $res = $this->db->update($this->table_name, $data['values'], ['id' => $autor->id], $data['formats'], '%d');

        }

        if ($res === false)
            return false;

        return $autor;
    }

    /**
     * @param $autor
     * @throws Books_Model_Exception
     */
    private function validate($autor)
    {
         // Check if object is valid
        if (!($autor instanceof Movie_Autor))
            throw new Books_Model_Exception("Saving invalid object!");

        $message = false;

        if (empty($autor->name))
            $message = "Author is not set!";

        if ($message !== false)
            throw new Books_Model_Exception($message);

    }
}
