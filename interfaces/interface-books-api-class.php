<?php 
interface Books_API_Class{

    /**
     * @return  array   Array representation of api object.
     */
    function to_api_object();
}