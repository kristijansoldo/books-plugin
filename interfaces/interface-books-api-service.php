<?php
interface Books_API_Service{

    /**
     * Sends object to api and handles response.
     *
     * @param $object
     * @return mixed
     */
    static function send_to_api($object);

    /**
     * Receives object from api and does
     * all actions to properly handle that response.
     *
     * @param $object
     * @return mixed
     */
    static function receive_from_api($object);

}