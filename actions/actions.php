<?php
class Books_Actions{
    public static $API_ERROR = 'l_api_error';
    public static $WP_SAVE_ERROR = 'l_wp_save_error';
    public static $WP_SYSTEM_ERROR = 'l_wp_system_error';
    public static $WP_CRON_ERROR = 'l_wp_cron_error';
}