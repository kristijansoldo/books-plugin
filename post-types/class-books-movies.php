<?php

class Books_Movie
{

    /**
     * Post type name.
     */
    const POST_TYPE = 'movie';

    /**
     * @var     int         Movie post id.
     */
    public $id;

    /**
     * @var     WP_Post     Movie post.
     */
    public $post;

    /**
     * Creates movie post type.
     */
    public static function register_post_type()
    {

        $labels = [
            'name'               => __('Movie'),
            'singular_name'      => __('Movie'),
            'menu_name'          => __('Movies'),
            'name_admin_bar'     => __('Movie'),
            'add_new'            => __('Add New'),
            'add_new_item'       => __('Add New Movie'),
            'new_item'           => __('New Movie'),
            'edit_item'          => __('Edit Movie'),
            'view_item'          => __('View Movie'),
            'all_items'          => __('All Movies'),
            'search_items'       => __('Search Movies'),
            'parent_item_colon'  => __('Parent Movies:'),
            'not_found'          => __('No movies found.'),
            'not_found_in_trash' => __('No movies found in Trash.'),
        ];

        $supports = ['title', 'editor', 'thumbnail'];

        $args = [
            'labels'      => $labels,
            'rewrite'     => ['slug' => 'movies'],
            'public'      => true,
            'has_archive' => true,
            'supports'    => $supports,
            'menu_icon'   => 'dashicons-clipboard',
        ];

        register_post_type(Books_Movie::POST_TYPE, $args);
        flush_rewrite_rules();

    }
    //dodavanje boxa za informacije
    public static function zanrovi_box_movies()
    {
        add_meta_box(
            'zanrovi_box_movies',
            __('Žanr i Autor', 'myplugin_textdomain'),
            array(__CLASS__, 'zanrovi_box_movies_content'),
            'movie',
            'side',
            'high'
        );
    }

    public static function zanrovi_box_movies_content($post)
    {
        global $post;
        wp_nonce_field(plugin_basename(__FILE__), 'zanrovi_box_content_movies_nonce');
        //dohvaćanje podataka za ispisvanje u value
        $zanr  = get_post_meta($post->ID, 'zanr', true);
        $autor = get_post_meta($post->ID, 'autor', true);
        //dohvaćanje dropdowna iz optionsa
        $zanrovi_get_option = get_option('zanorovi_novi');
        //pozivanje template-a boxa
        include BOOKS_PLUGIN_DIR . 'admin/partials/books-zanr-box.php';

    }

    public static function zanrovi_box_movies_save($post_id)
    {
        //autosejving
        if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
            return;
        }

        if (!empty($_POST['zanrovi_box_content_movies_nonce'])) {
            if (!wp_verify_nonce($_POST['zanrovi_box_content_movies_nonce'], plugin_basename(__FILE__))) {
                return;
            }

            if ('page' == $_POST['post_type']) {
                if (!current_user_can('edit_page', $post_id)) {
                    return;
                }

            } else {
                if (!current_user_can('edit_post', $post_id)) {
                    return;
                }

            }

            //dohvaćanje iz forme
            $zanr  = $_POST['zanr'];
            $autor = $_POST['autor'];

            //apdejtanje u bazu
            update_post_meta($post_id, 'zanr', $zanr);
            update_post_meta($post_id, 'autor', $autor);
        }
    }

}
